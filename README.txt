README.txt
==========

- Ever had more than one view on a page and wanted to get rid of duplicates?
While I couldn't find answer for that question I just wrote this module to help 
me out. And it does job pretty well.

I have a news site that has different blocks of views showing the same content 
type, some of them show promoted articles, while others show some other 
different variations. But sometimes it happens that few views show the same 
node in them as it matches those criteria. So to get rid of that problem I wrote
this simple module.

It gets node ids from the first view displayed on particular page, and alters 
query of the next view to remove those nodes from displaying.

COMPATIBILITY NOTES
==================

- Should work with all versions of Views, currently for Drupal 7 only

  AUTHOR/MAINTAINER
======================

- Tomasz Turczynski <tomasz AT turczynski DOT com>
  http://turczynski.com
